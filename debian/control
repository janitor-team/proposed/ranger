Source: ranger
Section: utils
Priority: optional
Maintainer: Vern Sun <s5unty@gmail.com>
Uploaders: Mo Zhou <lumin@debian.org>
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 13),
               dh-python,
               flake8,
               ncurses-bin (>= 5.7+20100313-5),
               pylint,
               python3-all,
               python3-pytest,
               shellcheck
Vcs-Git: https://salsa.debian.org/debian/ranger.git
Vcs-Browser: https://salsa.debian.org/debian/ranger
Homepage: https://ranger.github.io
Rules-Requires-Root: no

Package: ranger
Architecture: all
Depends: sensible-utils, ${misc:Depends}, ${python3:Depends}
Recommends: file, less, python3-chardet, w3m-img
Suggests: atool,
          caca-utils,
          elinks | elinks-lite | lynx | w3m,
          highlight | python3-pygments,
          mediainfo | exiftool,
          poppler-utils | mupdf-tools,
          sudo,
          unoconv
Description: Console File Manager with VI Key Bindings
 Ranger is a console file manager with VI key bindings.  It provides a
 minimalistic and nice curses interface with a view on the directory hierarchy.
 It ships with "rifle", a file launcher that is good at automatically finding
 out which program to use for what file type.
 .
 Design Goals
  * An easily maintainable file manager in a high level language
  * A quick way to switch directories and browse the file system
  * Keep it small but useful, do one thing and do it well
  * Console based, with smooth integration into the unix shell
