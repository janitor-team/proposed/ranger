#!/bin/sh

echo 1..2

. `dirname $0`/boilerplate.sh

ranger --version | fgrep -q 'ranger version:'
check_exit_code_true Outputs version

ranger --help | fgrep -q 'Options:'
check_exit_code_true Outputs help

