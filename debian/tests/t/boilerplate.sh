# -*- sh -*-

export TERM=linux
TESTNAME="`mktemp -t -u XXXXXXXX`"
RANGER="ranger -c "
count=0

check_exit_code_true() {
    if [ "$?" != 0 ]; then echo -n 'not '; fi; echo ok $(( count+=1 )) - "$@"
}

check_exit_code_false() {
    if [ "$?" = 0 ]; then echo -n 'not '; fi; echo ok $(( count+=1 )) - "$@"
}
